/**
 *  Copyright 2022 University of York
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.autofeedback.sample;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTimeoutPreemptively;
import static org.junit.jupiter.api.Assertions.fail;

import java.time.Duration;
import java.util.concurrent.Callable;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

public class RomanNumberTest {

	private RomanNumberParser converter;

	@BeforeEach
	public void setup() {
		this.converter = new RomanNumberParser();
	}

	@Test
	public void emptyString() {
		assertThrowsWithTimeout(IllegalArgumentException.class, () -> fromRoman(""), "An empty string should be rejected");
	}

	@Test
	public void ones() {
		assertTimeoutPreemptively(Duration.ofSeconds(1), () -> assertEquals(1, fromRoman("I"), "fromRoman(I) = 1"));
		
		assertEqualsWithTimeout(2, () -> fromRoman("II"), "fromRoman(II) = 2");
		assertEqualsWithTimeout(3, () -> fromRoman("III"), "fromRoman(III) = 3");
		assertThrowsWithTimeout(IllegalArgumentException.class, () -> fromRoman("IIII"), "fromRoman(IIII) should be rejected");
	}

	@Test
	public void vLetter() {
		assertEqualsWithTimeout(4, () -> fromRoman("IV"), "fromRoman(IV) = 4");
		assertEqualsWithTimeout(5, () -> fromRoman("V"), "fromRoman(V) = 5");
		assertEqualsWithTimeout(6, () -> fromRoman("VI"), "fromRoman(VI) = 6");
		assertEqualsWithTimeout(7, () -> fromRoman("VII"), "fromRoman(VII) = 7");
		assertEqualsWithTimeout(8, () -> fromRoman("VIII"), "fromRoman(VIII) = 8");
		assertThrowsWithTimeout(IllegalArgumentException.class, () -> fromRoman("VIIII"), "fromRoman(VIIII) should be rejected");
		assertThrowsWithTimeout(IllegalArgumentException.class, () -> fromRoman("IVI"), "fromRoman(IVI) should be rejected");
		assertThrowsWithTimeout(IllegalArgumentException.class, () -> fromRoman("IVII"), "fromRoman(IVII) should be rejected");
		assertThrowsWithTimeout(IllegalArgumentException.class, () -> fromRoman("IVIII"), "fromRoman(IVIII) should be rejected");
	}

	@Test
	public void xUnits() {
		assertEqualsWithTimeout(9, () -> fromRoman("IX"), "fromRoman(IX) = 9");
		assertThrowsWithTimeout(IllegalArgumentException.class, () -> fromRoman("IXI"), "fromRoman(IXI) should be rejected");
	}

	@Test
	public void xTens() {
		assertEqualsWithTimeout(10, () -> fromRoman("X"), "fromRoman(X) = 10");
		assertEqualsWithTimeout(20, () -> fromRoman("XX"), "fromRoman(XX) = 20");
		assertEqualsWithTimeout(30, () -> fromRoman("XXX"), "fromRoman(XXX) = 30");
		assertThrowsWithTimeout(IllegalArgumentException.class, () -> fromRoman("XXXX"), "fromRoman(XXXX) should be rejected");
	}

	@Test
	public void lTens() {
		assertEqualsWithTimeout(40, () -> fromRoman("XL"), "fromRoman(XL) = 40");
		assertEqualsWithTimeout(50, () -> fromRoman("L"), "fromRoman(L) = 50");
		assertEqualsWithTimeout(60, () -> fromRoman("LX"), "fromRoman(LX) = 60");
		assertEqualsWithTimeout(70, () -> fromRoman("LXX"), "fromRoman(LXX) = 70");
		assertEqualsWithTimeout(80, () -> fromRoman("LXXX"), "fromRoman(LXXX) = 80");
		assertThrowsWithTimeout(IllegalArgumentException.class, () -> fromRoman("XXL"), "fromRoman(XXL) should be rejected");
		assertThrowsWithTimeout(IllegalArgumentException.class, () -> fromRoman("LXXXX"), "fromRoman(LXXXX) should be rejected");
	}

	@Test
	public void cTens() {
		assertEqualsWithTimeout(90, () -> fromRoman("XC"), "fromRoman(XC) = 90");
		assertThrowsWithTimeout(IllegalArgumentException.class, () -> fromRoman("XCX"), "fromRoman(XCX) should be rejected");
	}

	@Test
	public void cHundreds() {
		assertEqualsWithTimeout(100, () -> fromRoman("C"), "fromRoman(C) = 100");
		assertEqualsWithTimeout(200, () -> fromRoman("CC"), "fromRoman(CC) = 200");
		assertEqualsWithTimeout(300, () -> fromRoman("CCC"), "fromRoman(CCC) = 300");
		assertThrowsWithTimeout(IllegalArgumentException.class, () -> fromRoman("CCCC"), "fromRoman(CCCC) should be rejected");
	}

	@Test
	public void dLetter() {
		assertEqualsWithTimeout(400, () -> fromRoman("CD"), "fromRoman(C) = 400");
		assertEqualsWithTimeout(500, () -> fromRoman("D"), "fromRoman(CC) = 500");
		assertEqualsWithTimeout(600, () -> fromRoman("DC"), "fromRoman(CCC) = 600");
		assertEqualsWithTimeout(700, () -> fromRoman("DCC"), "fromRoman(CCC) = 700");
		assertEqualsWithTimeout(800, () -> fromRoman("DCCC"), "fromRoman(CCC) = 800");
		assertThrowsWithTimeout(IllegalArgumentException.class, () -> fromRoman("DCCCC"), "fromRoman(DCCCC) should be rejected");
	}

	@Test
	public void mHundreds() {
		assertEqualsWithTimeout(900, () -> fromRoman("CM"), "fromRoman(CM) = 900");
		assertThrowsWithTimeout(IllegalArgumentException.class, () -> fromRoman("CCM"), "fromRoman(CCM) should be rejected");
	}

	@Test
	public void mThousands() {
		assertEqualsWithTimeout(1000, () -> fromRoman("M"), "fromRoman(M) = 1000");
		assertEqualsWithTimeout(2000, () -> fromRoman("MM"), "fromRoman(MM) = 2000");
		assertEqualsWithTimeout(3000, () -> fromRoman("MMM"), "fromRoman(MMM) = 3000");
		assertThrowsWithTimeout(IllegalArgumentException.class, () -> fromRoman("MMMM"), "fromRoman(MMMM) should be rejected");
	}

	@Test
	public void toRomanRanges() {
		assertThrowsWithTimeout(IllegalArgumentException.class, () -> toRoman(-10), "Cannot represent negative integers as Roman numbers");
		assertThrowsWithTimeout(IllegalArgumentException.class, () -> toRoman(0), "Cannot represent zero as a Roman number");
		assertThrowsWithTimeout(IllegalArgumentException.class, () -> toRoman(4000), "Cannot represent numbers greater than or equal to 4000 as a Roman number");
	}

	@Test
	public void toRomanSample() {
		int[] from = {1, 5, 10, 20, 50, 100, 500, 1250};
		String[] to = {"I", "V", "X", "XX", "L", "C", "D", "MCCL"};
		assert from.length == to.length;

		for (int i = 0; i < from.length; i++) {
			final int pos = i;
			assertEqualsWithTimeout(to[i], () -> toRoman(from[pos]), String.format("toRoman(%d) should produce '%s'", from[i], to[i]));
		}
	}

	@Test
	public void exhaustiveRoundtrip() {
		for (int i = RomanNumberParser.MIN_VALUE; i < RomanNumberParser.MAX_VALUE; i++) {
			final int n = i;
			assertTimeoutPreemptively(Duration.ofSeconds(1), () -> {
				final String roman = toRoman(n);
				try {
					final int arabic = fromRoman(roman);
					assertEquals(n, arabic, String.format(
						"Converting %d to Roman and back to an Arabic number should return the original number (was converted to Roman as %s",
						n, roman));
				} catch (Exception ex) {
					fail(String.format("Failed to convert %d to Roman and back (was converted to Roman as '%s'): %s",
						n, roman, ex.getMessage()));
				}
			}, String.format("It should be possible to convert %d into a Roman number and back to itself", i));
		}
	}

	private <T> void assertEqualsWithTimeout(T expected, Callable<T> exec, String msg) {
		assertTimeoutPreemptively(Duration.ofSeconds(1), () -> assertEquals(expected, exec.call(), msg), msg);
	}

	private <T extends Throwable> void assertThrowsWithTimeout(Class<T> expectedException, Executable exec, String msg) {
		assertTimeoutPreemptively(Duration.ofSeconds(1), () -> assertThrows(expectedException, exec, msg), msg);
	}

	private int fromRoman(String roman) {
		return converter.fromRoman(roman);
	}

	private String toRoman(int n) {
		return converter.toRoman(n);
	}
}