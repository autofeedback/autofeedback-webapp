<?php

/**
 *  Copyright 2021 Aston University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace App\Ldap\Rules;

use App\User;
use Illuminate\Database\Eloquent\Model as Eloquent;
use LdapRecord\Laravel\Auth\Rule;
use LdapRecord\Models\Model as LdapRecord;

class WebLoginIsNotDisabled implements Rule
{
    /**
     * Check if the rule passes validation.
     *
     * @param LdapRecord $user
     * @param Eloquent|null $model
     * @return bool
     */
    public function passes(LdapRecord $user, Eloquent $model = null): bool
    {
        /** @var User $model */
        return !$model->disabled_login;
    }
}
