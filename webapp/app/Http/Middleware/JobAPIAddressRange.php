<?php

/**
 *  Copyright 2020-2021 Aston University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace App\Http\Middleware;

use App\Http\Controllers\API\GeneratesJSON;
use Closure;
use Illuminate\Http\Request;

class JobAPIAddressRange
{
    use GeneratesJSON;

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $ranges = explode(',', config('app.job_api_ip_range'));
        if (strpos($request->ip(), ":") === FALSE) {
            $check = function ($ip, $range) {
                return ipv4_in_range($ip, $range);
            };
        } else {
            $check = function ($ip, $range) {
                return ipv6_in_range($ip, $range);
            };
        }

        foreach ($ranges as $range) {
            if ($check($request->ip(), $range)) {
                return $next($request);
            }
        }

        return $this->jsonForbidden("Unauthorised address",
            "Your IP address does not have access to this API.");
    }
}
