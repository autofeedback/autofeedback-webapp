@php
    /**
     *  Copyright 2022 Aston University
     *
     * Licensed under the Apache License, Version 2.0 (the "License");
     * you may not use this file except in compliance with the License.
     * You may obtain a copy of the License at
     *
     * http://www.apache.org/licenses/LICENSE-2.0
     *
     * Unless required by applicable law or agreed to in writing, software
     * distributed under the License is distributed on an "AS IS" BASIS,
     * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
     * See the License for the specific language governing permissions and
     * limitations under the License.
     */
@endphp

@extends('layouts.app')

@php /** @var \App\TeachingModuleItem $item */ @endphp

@section('title')
    {{ __('Copy Item')}}
@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <x-item-path :module="$module" :item="$item">
                <li class="breadcrumb-item active">{{ __('Copy Item') }}</li>
            </x-item-path>

            <form method="POST"
                  action="{{ route('modules.items.copy', ['module' => $module, 'item' => $item ]) }}">
                @csrf

                <div class="form-group row">
                    <label for="targetPath">{{ __('Target path') }}</label>
                    <select name="targetPath" id="targetPath" class="form-control">
                        @foreach ($targetFolders as $target)
                        <option value="{{ $target['module']->id . '|' . ($target['item'] ? $target['item']->id : '') }}">{{ $target['path'] }}</option>
                        @endforeach
                    </select>
                    @error('targetPath')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <div class="text-center">
                    <input class="btn btn-primary" type="submit" value="{{ __('Submit') }}">
                    <a role="button" class="btn btn-secondary"
                       @if($folder) href="{{ route('modules.items.show', ['module' => $module, 'item' => $container]) }}"
                       @else href="{{ route('modules.show', $module->id) }}" @endif
                    >{{ __('Cancel') }}</a>
                </div>
            </form>
        </div>
    </div>
@endsection
