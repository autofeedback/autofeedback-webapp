#!/bin/sh

# From 'Tips and Tricks of the Docker Captains' (Docker channel, YouTube)
# https://youtu.be/woBI466WMR8

if [ "$(id -u)" = "0" ]; then
    # running on a dev machine: fix perms then run as non-privileged user
    fix-perms -r -u www-data -g tty /app
    gosu www-data:tty mkdir -p \
        /app/storage/framework/cache/data \
        /app/storage/framework/sessions \
        /app/storage/framework/views \
        /app/storage/logs

    exec gosu www-data:tty /usr/bin/tini-static -- "$@"
else
    # run as usual (note: production doesn't use this entrypoint)
    exec /usr/bin/tini-static -- "$@"
fi
